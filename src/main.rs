use serenity::{
    async_trait,
    client::{
        bridge::{gateway::GatewayIntents, gateway::ShardManager},
        EventHandler,
    },
    framework::standard::{
        macros::{command, group, hook},
        CommandError, CommandResult, StandardFramework,
    },
    http::Http,
    model::{
        channel::{Message, Reaction},
        id::GuildId,
    },
    prelude::{Client, Context, Mutex, TypeMapKey},
};

use std::{collections::HashSet, convert::TryInto, fs::File, io::prelude::*, sync::Arc};

use sqlx::postgres::{PgPool, PgPoolOptions};
use toml::Value;

use tracing::{info, Level};

use tracing_log::LogTracer;
use tracing_subscriber::FmtSubscriber;

mod utils;

mod commands;
use commands::moderation::*;
use commands::reactionroles::*;

struct ShardManagerContainer;
struct DatabasePoolContainer;
struct ReactionMessagesContainer;

impl TypeMapKey for ShardManagerContainer {
    type Value = Arc<Mutex<ShardManager>>;
}
impl TypeMapKey for DatabasePoolContainer {
    type Value = PgPool;
}
impl TypeMapKey for ReactionMessagesContainer {
    type Value = Vec<u64>;
}
struct Handler;

#[command]
async fn ping(ctx: &Context, msg: &Message) -> CommandResult {
    msg.channel_id.say(ctx, "Pong!").await?;
    Ok(())
}

#[async_trait]
impl EventHandler for Handler {
    async fn reaction_add(&self, ctx: Context, reaction: Reaction) {
        if let Err(e) = handle_reaction(ctx, reaction, true).await {
            eprintln!("An error ocurred in handle_reaction {}", e);
        }
    }
    async fn reaction_remove(&self, ctx: Context, reaction: Reaction) {
        if let Err(e) = handle_reaction(ctx, reaction, false).await {
            eprintln!("An error ocurred in handle_reaction {}", e);
        }
    }
    async fn cache_ready(&self, _ctx: Context, _: Vec<GuildId>) {
        println!("Cache ready!");
    }
}

#[hook]
async fn after(_ctx: &Context, _msg: &Message, _cmd_name: &str, res: Result<(), CommandError>) {
    if let Err(e) = res {
        eprintln!("{}", e);
    }
}

#[group("Meta")]
#[commands(ping)]
struct Meta;

#[group("reactionroles")]
#[commands(rr)]
struct ReactionRoles;

#[group("Moderation")]
#[commands(slowmode)]
struct Moderation;

/*
#[group("Music")]
#[only_in("guilds")]
#[commands(join, play, leave, volume, skip, queue, now_playing)]
struct Music;
*/

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let config = {
        let mut file = File::open("config.toml")?;
        let mut contents = String::new();
        file.read_to_string(&mut contents)?;
        contents.parse::<Value>()?
    };

    let token = config["token"].as_str().expect("No bot token specified");
    let prefix = config["prefix"].as_str().unwrap_or("b!");
    let db_bind = config["db-bind"]
        .as_str()
        .expect("No database url specified");
    let tracing_enable = config
        .get("enable_tracing")
        .unwrap_or(&toml::Value::Boolean(false))
        .as_bool()
        .unwrap();
    let tracing_level = config["trace_level"].as_str().unwrap_or("error");
    if tracing_enable {
        LogTracer::init()?;
        let level = match tracing_level {
            "error" => Level::ERROR,
            "warn" => Level::WARN,
            "info" => Level::INFO,
            "debug" => Level::DEBUG,
            "trace" => Level::TRACE,
            _ => Level::TRACE,
        };

        info!("Tracer initialized.");

        let subscriber = FmtSubscriber::builder().with_max_level(level).finish();
        tracing::subscriber::set_global_default(subscriber)?;
    }

    let http = Http::new_with_token(&token);

    let owners = match http.get_current_application_info().await {
        Ok(info) => {
            let mut owners = HashSet::new();
            owners.insert(info.owner.id);
            owners
        }
        Err(why) => panic!("Couldn't fetch application info {:?}", why),
    };

    let framework = StandardFramework::new()
        .configure(|c| c.with_whitespace(false).prefix(prefix).owners(owners))
        .after(after)
        .group(&REACTIONROLES_GROUP)
        .group(&MODERATION_GROUP)
        .group(&META_GROUP);

    let mut client = Client::builder(&token)
        .framework(framework)
        .event_handler(Handler)
        .intents(GatewayIntents::all())
        .await?;
    client.cache_and_http.cache.set_max_messages(100).await;

    {
        let mut data = client.data.write().await;
        let pool = PgPoolOptions::new()
            .max_connections(5)
            .connect(db_bind)
            .await?;
        let reaction_messages: Vec<u64> =
            sqlx::query_scalar("SELECT message_id FROM reaction_roles")
                .fetch_all(&pool)
                .await
                .unwrap()
                .into_iter()
                .map(|x: i64| x.try_into().unwrap())
                .collect();
        data.insert::<DatabasePoolContainer>(pool);
        data.insert::<ShardManagerContainer>(Arc::clone(&client.shard_manager));
        data.insert::<ReactionMessagesContainer>(reaction_messages);
    }

    if let Err(e) = client.start().await {
        eprintln!("An error ocurred while starting the bot {:?}", e);
    };

    Ok(())
}
