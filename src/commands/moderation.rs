use serde_json::{Map, Value};
use serenity::{
    framework::standard::{macros::command, Args, CommandResult},
    model::channel::{Message, ReactionType},
    prelude::Context,
};

#[command]
#[required_permissions(MANAGE_CHANNELS)]
#[aliases("sm")]
async fn slowmode(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let amount = if let Some(a) = args.current() {
        if a.to_lowercase() == "off" {
            0i64
        } else {
            match a.parse::<i64>() {
                Ok(i) => i,
                Err(_) => {
                    let _ = msg
                        .react(&ctx, ReactionType::Unicode(String::from("❌")))
                        .await;
                    return Err("Invalid input".into());
                }
            }
        }
    } else {
        let _ = msg
            .react(&ctx, ReactionType::Unicode(String::from("❌")))
            .await;
        return Err("Missing argument".into());
    };
    let mut map: Map<String, Value> = Map::new();
    map.insert(
        String::from("rate_limit_per_user"),
        Value::Number(serde_json::Number::from(amount)),
    );
    let _ = match ctx.http.edit_channel(msg.channel_id.0, &map).await {
        Ok(_) => {
            msg.react(&ctx, ReactionType::Unicode(String::from("✅")))
                .await
        }
        Err(_) => {
            msg.react(&ctx, ReactionType::Unicode(String::from("❌")))
                .await
        }
    };
    Ok(())
}
