use serenity::{
    framework::standard::{macros::command, CommandResult},
    model::{
        channel::{GuildChannel, Message, Reaction, ReactionType},
        id::MessageId,
    },
    prelude::{Context, Mentionable},
    utils::Color,
};

use std::{collections::HashMap, convert::TryInto, sync::Arc, time::Duration};

use crate::{
    utils::parsers::{parse_channel, parse_role},
    DatabasePoolContainer, ReactionMessagesContainer,
};

use unic_emoji_char::is_emoji;

#[derive(Debug)]
struct PartialMessage {
    id: MessageId,
    content: String,
}

pub async fn handle_reaction(ctx: Context, reaction: Reaction, added: bool) -> CommandResult {
    if ctx
        .data
        .read()
        .await
        .get::<ReactionMessagesContainer>()
        .unwrap()
        .contains(&reaction.message_id.0)
    {
        let reaction_info = {
            let data = ctx.data.read().await;
            let pool = data.get::<DatabasePoolContainer>().unwrap();
            let info = sqlx::query!(
                "SELECT * FROM reaction_roles WHERE message_id = $1",
                reaction.message_id.0 as i64
            )
            .fetch_optional(pool)
            .await?;
            match info {
                Some(i) => i,
                None => return Ok(()),
            }
        };
        let roles_map = reaction_info
            .emojis
            .into_iter()
            .zip(reaction_info.role_ids)
            .collect::<indexmap::IndexMap<String, i64>>();
        let guild_id = reaction
            .guild_id
            .ok_or_else(|| "Missing guild id in handle_reaction")?
            .0;
        let user_id = reaction
            .user_id
            .ok_or_else(|| "Missing user_id in handle_reaction")?
            .0;

        let emoji = match &reaction.emoji {
            ReactionType::Unicode(e) => e,
            #[allow(unused_variables)]
            ReactionType::Custom { name, id, animated } => {
                return Ok(());
            }
            _ => return Err("Invalid emoji".into()),
        };

        if let Some(r) = roles_map.get(emoji) {
            let role_u64: u64 = r.clone().try_into().unwrap();
            match added {
                true => ctx.http.add_member_role(guild_id, user_id, role_u64).await,
                false => {
                    ctx.http
                        .remove_member_role(guild_id, user_id, role_u64)
                        .await
                }
            }?;
        }
    }
    Ok(())
}

macro_rules! wait_for_message_content {
    ($ctx:expr, $channel_id:expr, $author:expr) => {{
        let author_id = $author.id;
        let channel_id = $channel_id;
        let filter =
            move |msg: &Arc<Message>| msg.author.id == author_id && msg.channel_id == channel_id;
        let ret = $author
            .await_reply($ctx)
            .filter(filter)
            .timeout(Duration::from_secs(30))
            .await;
        match ret {
            Some(m) => {
                if m.content == "cancel" {
                    $channel_id.say($ctx, "Cancelling...".to_string()).await?;
                    return Ok(());
                }
                PartialMessage {
                    id: m.id,
                    content: m.content.clone(),
                }
            }
            None => {
                $channel_id.say($ctx, "Timed out").await?;
                return Ok(());
            }
        }
    }};
}

#[command]
#[aliases("reactionroles")]
#[only_in("guilds")]
async fn rr(ctx: &Context, msg: &Message) -> CommandResult {
    let channel_id = msg.channel_id;
    let author = ctx
        .http
        .get_member(msg.guild_id.unwrap().0, msg.author.id.0)
        .await
        .unwrap();
    let perms = author.permissions(ctx).await.expect("perms");
    if !perms.manage_roles() {
        return Ok(());
    }
    channel_id
        .say(&ctx, "Sup, what channel do you want the message to be in?")
        .await?;

    let reaction_channel: GuildChannel = loop {
        let message = wait_for_message_content!(&ctx, channel_id, author.user);

        match parse_channel(&ctx, msg, &message.content).await {
            Ok(c) => break c,
            Err(e) => {
                channel_id.say(&ctx, e.to_string()).await?;
                continue;
            }
        };
    };
    channel_id.say(&ctx, format!("Nice, the channel is going to be {}. What would you like the message to say? Use | to seperate the embed title from the description, like this ```Title | Description``` You can also type `{{r}}` to have it replaced by a list of each emoji and its role.", reaction_channel.mention())).await?;
    let (title, description): (String, Option<String>) = loop {
        let message = wait_for_message_content!(&ctx, channel_id, author.user);

        let split_vec = message.content.split("|").collect::<Vec<&str>>();

        match split_vec.len() {
            1 => break (split_vec[0].to_string(), None),
            2 => break (split_vec[0].to_string(), Some(split_vec[1].to_string())),
            _ => {
                channel_id
                    .say(&ctx, "You used more than one |, try again")
                    .await?;
                continue;
            }
        }
    };
    channel_id
        .say(
            &ctx,
            "Great, do you want the embed to have a specific hex color?",
        )
        .await?;
    let color = loop {
        let message = wait_for_message_content!(&ctx, channel_id, author.user);
        match u32::from_str_radix(
            &message
                .content
                .strip_prefix("0x")
                .or_else(|| message.content.strip_prefix("#"))
                .unwrap_or(&message.content),
            16,
        ) {
            Ok(c) => break Some(Color::from(c)),
            Err(_) => {
                channel_id
                    .say(&ctx, "Thats not a valid hex code, try again")
                    .await?;
                continue;
            }
        }
    };

    channel_id.send_message(&ctx, |m| {
        m.content("This is how the message is going to look like. Now we need to add the roles\n*Example:*```:red_car: Rocket league```When you are done, type `done`");
        m.embed(|e| {
            e.title(&title);
            if let Some(c) = color {
                e.color(c);
            }
            if let Some(d) = &description {
                e.description(d);
            }
            e
        });
        m
    }).await?;

    let mut roles: HashMap<String, i64> = HashMap::new();

    loop {
        let message = wait_for_message_content!(&ctx, msg.channel_id, msg.author);

        if message.content.to_lowercase() == "done" {
            ctx.http
                .create_reaction(
                    channel_id.0,
                    message.id.0,
                    &ReactionType::Unicode(String::from("✅")),
                )
                .await?;
            break;
        }

        let index = match message.content.find(" ") {
            Some(i) => i,
            None => {
                channel_id.say(&ctx, "missing role, try again").await?;
                continue;
            }
        };

        let (emoji, role_raw) = message.content.split_at(index);
        let role_id_res = parse_role(ctx, &msg.guild_id.unwrap(), role_raw.trim_start()).await;

        if !is_emoji(emoji.chars().nth(0).unwrap()) {
            channel_id.say(&ctx, "Invalid emoji, try again").await?;
            continue;
        }

        let role_id = match role_id_res {
            Ok(r) => r,
            Err(e) => {
                channel_id.say(&ctx, e).await?;
                continue;
            }
        };

        roles.insert(emoji.to_string(), role_id as i64);
        ctx.http
            .create_reaction(
                msg.channel_id.0,
                message.id.0,
                &ReactionType::Unicode(String::from("✅")),
            )
            .await?;
    }

    let role_list = if let Some(d) = description.clone() {
        if d.contains("{r}") {
            let mut d_roles = String::from("\n");
            for (emoji, role_id) in &roles {
                d_roles.push_str(format!("{} <@&{}>\n", emoji, role_id).as_str())
            }
            Some(d_roles)
        } else {
            Some(d)
        }
    } else {
        None
    };

    let message = reaction_channel
        .send_message(&ctx, |m| {
            m.embed(|e| {
                e.title(title);
                if let Some(c) = color {
                    e.color(c);
                }
                if let Some(d) = role_list {
                    e.description(
                        description
                            .unwrap_or(String::from(""))
                            .replace("{r}", d.as_str()),
                    );
                }
                e
            });
            m
        })
        .await?;
    for emoji in roles.keys() {
        message
            .react(&ctx, ReactionType::Unicode(emoji.clone()))
            .await?;
    }

    let (emoji_vec, role_vec): (Vec<String>, Vec<i64>) = roles.into_iter().unzip();

    {
        let data = ctx.data.read().await;
        let pool = data.get::<DatabasePoolContainer>().unwrap();
        sqlx::query!(
            "INSERT INTO reaction_roles VALUES ($1, $2, $3)",
            message.id.0 as i64,
            emoji_vec.as_slice(),
            role_vec.as_slice()
        )
        .execute(pool)
        .await?;
    }
    {
        let mut data = ctx.data.write().await;
        let reaction_messages = data.get_mut::<crate::ReactionMessagesContainer>().unwrap();
        reaction_messages.push(message.id.0);
    }

    Ok(())
}
