use regex::Regex;
use serenity::{
    model::{
        channel::{GuildChannel, Message},
        id::{ChannelId, GuildId, RoleId},
    },
    prelude::Context,
};

pub async fn parse_channel(
    ctx: &Context,
    msg: &Message,
    channel_raw: &String,
) -> Result<GuildChannel, String> {
    if let Ok(c) = channel_raw.parse::<ChannelId>() {
        let channel = c.to_channel(ctx).await;
        match channel {
            Ok(c) => Ok(c.guild().unwrap()),
            Err(why) => Err(why.to_string()),
        }
    } else if channel_raw.starts_with("<#") && channel_raw.ends_with('>') {
        let re = Regex::new("[<#!>]").unwrap();
        let channel_id = re.replace_all(&channel_raw, "").into_owned();
        let channel = channel_id
            .parse::<ChannelId>()
            .unwrap()
            .to_channel(ctx)
            .await;
        match channel {
            Ok(c) => Ok(c.guild().unwrap()),
            Err(why) => Err(why.to_string()),
        }
    } else {
        let guild = &msg.guild(ctx).await.unwrap();
        let channel_id = guild.channel_id_from_name(ctx, &channel_raw).await;
        match channel_id {
            Some(c) => Ok(c.to_channel(ctx).await.unwrap().guild().unwrap()),
            None => Err(String::from("Couldn't find a channel with that name or id")),
        }
    }
}

pub async fn parse_role<'a>(
    ctx: &'a Context,
    gid: &GuildId,
    role_raw: &str,
) -> Result<u64, &'a str> {
    let guild = gid.to_guild_cached(&ctx).await.unwrap();
    if let Ok(r) = role_raw.parse::<RoleId>() {
        if guild.roles.contains_key(&r) {
            Ok(r.0)
        } else {
            Err("No role with this id or name found")
        }
    } else if role_raw.starts_with("<@&") && role_raw.ends_with(">") {
        let re = Regex::new("[<@&>]").unwrap();
        let role_id = re
            .replace_all(&role_raw, "")
            .into_owned()
            .parse::<RoleId>()
            .unwrap();
        if guild.roles.contains_key(&role_id) {
            Ok(role_id.0)
        } else {
            Err("No role with this id or name found")
        }
    } else {
        match guild.role_by_name(role_raw).map(|r| r.id) {
            Some(r) => Ok(r.clone().0),
            None => Err("No role with this id or name found"),
        }
    }
}
